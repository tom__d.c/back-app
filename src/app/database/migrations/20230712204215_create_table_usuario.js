
exports.up = knex => {
  return knex.schema.createTable('usuario', table => {
    table.increments('id')
    table.text('nome').notNullable()
    table.text('email').notNullable().unique()
    table.string('celular').notNullable().unique()
    table.text('senha').notNullable()
    table.string('status').notNullable()
    table.string('grupo').notNullable()
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
  });
};

exports.down = knex => knex.schema.dropTable('usuario');
