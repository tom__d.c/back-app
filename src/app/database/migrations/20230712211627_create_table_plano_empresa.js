
exports.up = knex => {
  return knex.schema.createTable('plano_empresa', table => {
    table.increments('id')
    table.text('nome').notNullable()
    table.text('descricao').notNullable()
    table.string('valor').notNullable()
    table.string('desconto').notNullable()
    table.string('status').notNullable()
    table.integer('empresa_id')
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
  });
};

exports.down = knex => knex.schema.dropTable('plano_empresa');
