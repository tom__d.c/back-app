
exports.up = knex => {
    return knex.schema.createTable('endereco', table => {
        table.increments('id')
        table.text('logradouro').notNullable()
        table.integer('numero').notNullable()
        table.string('bairro').notNullable()
        table.string('cep').notNullable()
        table.string('cidade').notNullable()
        table.string('estado').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
};

exports.down = knex => knex.schema.dropTable('endereco')
