
exports.up = knex => {
    return knex.schema.createTable('cartao', table => {
        table.increments('id')
        table.string('numero').notNullable()
        table.string('cpf').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
}


exports.down = knex => knex.schema.dropTable('cartao');
