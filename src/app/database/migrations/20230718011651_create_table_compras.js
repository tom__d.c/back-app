
exports.up = knex => {
    return knex.schema.createTable('compras', table => {
        table.increments('id')
        table.integer('empresa_id')
        table.integer('cliente_id')
        table.string('valor').notNullable()
        table.integer('cupom_utilizado_id').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
}


exports.down = knex => knex.schema.dropTable('compras');
