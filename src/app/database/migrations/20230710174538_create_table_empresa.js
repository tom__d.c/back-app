
exports.up = (knex) => {
  return knex.schema.createTable('empresa', table => {
    table.increments('id')
    table.text('razao_social').notNullable()
    table.text('nome_fantasia').notNullable()
    table.string('cnpj').notNullable()
    table.text('email').notNullable().unique()
    table.string('telefone1').notNullable()
    table.string('telefone2').notNullable()
    table.text('empresa').notNullable()
    table.string('whatsapp').notNullable()
    table.integer('endereco_id')
    table.integer('usuario_id')
    table.integer('plano_sistema_id')

    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('update_at').defaultTo(knex.fn.now())
  });
};

exports.down = knex => knex.schema.dropTable('empresa');
