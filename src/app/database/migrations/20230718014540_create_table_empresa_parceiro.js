
exports.up = knex => {
    return knex.schema.createTable('empresa_parceiro', table => {
        table.increments('id')
        table.integer('empresa_id')
        table.integer('parceiro_id')
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
}


exports.down = knex => knex.schema.dropTable('empresa_parceiro');
