
exports.up = knex => {
  return knex.schema.createTable('grupo', table => {
    table.increments('id')
    table.string('nome').notNullable()
    table.text('descricao').notNullable()
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
  });
};

exports.down = knex => knex.schema.dropTable('grupo');
