
exports.up = knex => {
  return knex.schema.createTable('cupon', table => {
    table.increments('id')
    table.string('titulo').notNullable()
    table.text('descricao').notNullable()
    table.string('desconto').notNullable()
    table.string('status').notNullable()
    table.string('publico').notNullable()
    table.integer('empresa_id')
    table.integer('usuario_id')
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
  });
};

exports.down = knex => knex.schema.dropTable('cupon');
