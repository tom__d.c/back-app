
exports.up = knex => {
    return knex.schema.createTable('ponto', table => {
        table.increments('id')
        table.integer('acumulado')
        table.integer('cliente_id')
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
};

exports.down = knex => knex.schema.dropTable('ponto');
