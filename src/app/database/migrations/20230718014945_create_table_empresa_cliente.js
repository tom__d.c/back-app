
exports.up = knex => {
    return knex.schema.createTable('empresa_cliente', table => {
        table.increments('id')
        table.integer('plano_empresa_id')
        table.integer('cliente_id')
        table.string('status').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    });
}


exports.down = knex => knex.schema.dropTable('empresa_cliente');

