
exports.up = knex => {
  return knex.schema.createTable('cliente', table => {
    table.increments('id')
    table.string('nome').notNullable()
    table.string('cpf').unique().notNullable()
    table.string('rg').unique().notNullable()
    table.string('sexo').notNullable()
    table.text('email').unique().notNullable()
    table.string('celular').notNullable()
    table.string('telefone').notNullable()
    table.string('whatsapp')
    table.integer('titular_id')
    table.integer('usuario_id')
    table.integer('endereco_id')
    table.integer('cartao_id')
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())    
  });
};

exports.down = knex => knex.schema.dropTable('cliente');
