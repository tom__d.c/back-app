const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { empresa_id, parceiro_id, cupon_id } = req.body

            await knex('cupon_empresa').insert({
                empresa_id,
                parceiro_id,
                cupon_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('cupon_empresa')
        return results
    },

    async findOneById(id) {
        const result = knex('cupon_empresa').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { empresa_id, parceiro_id, cupon_id } = data

            await knex('cupon_empresa')
                .where('id', id)
                .update({
                    empresa_id: empresa_id,
                    parceiro_id: parceiro_id,
                    cupon_id: cupon_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('cupon_empresa').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}