const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { nome, descricao, valor, desconto, status } = req.body

            await knex('plano_sistema').insert({
                nome,
                descricao,
                valor,
                desconto,
                status,        
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('plano_sistema')
        return results
    },

    async findOneById(id) {
        const result = knex('plano_sistema').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { nome, descricao, valor, desconto, status } = data

            await knex('plano_sistema')
                .where('id', id)
                .update({
                    nome: nome,
                    descricao: descricao,
                    valor: valor,
                    desconto: desconto,
                    status: status,
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('plano_sistema').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}