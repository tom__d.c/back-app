const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { nome, data_nascimento, cpf, rg, sexo, email, celular, telefone, whatsapp,
            titular_id, usuario_id, endereco_id, cartao_id } = req.body

            await knex('cliente').insert({
                nome, 
                data_nascimento, 
                cpf, 
                rg, 
                sexo, 
                email, 
                celular, 
                telefone, 
                whatsapp,
                titular_id, 
                usuario_id, 
                endereco_id, 
                cartao_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('cliente')
        return results
    },

    async findOneById(id) {
        const result = knex('cliente').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { nome, data_nascimento, cpf, rg, sexo, email, celular, telefone, whatsapp,
                titular_id, usuario_id, endereco_id, cartao_id } = data

            await knex('cliente')
                .where('id', id)
                .update({
                    nome: nome, 
                    data_nascimento: data_nascimento, 
                    cpf: cpf, 
                    rg: rg, 
                    sexo: sexo, 
                    email: email, 
                    celular: celular, 
                    telefone: telefone, 
                    whatsapp: whatsapp,
                    titular_id: titular_id, 
                    usuario_id: usuario_id, 
                    endereco_id: endereco_id, 
                    cartao_id: cartao_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next){
        try {
            knex('cliente').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}