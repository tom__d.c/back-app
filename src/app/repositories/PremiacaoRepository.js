const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { nome, descricao, valor, status, empresa_id } = req.body

            await knex('premiacao').insert({
                nome,
                descricao,
                valor,
                status,
                empresa_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('premiacao')
        return results
    },

    async findOneById(id) {
        const result = knex('premiacao').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { nome, descricao, valor, status, empresa_id } = data

            await knex('premiacao')
                .where('id', id)
                .update({
                    nome: nome,
                    descricao: descricao,
                    valor: valor,
                    status: status,
                    empresa_id: empresa_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('premiacao').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}