const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { nome, descricao } = req.body

            await knex('grupo').insert({
                nome,
                descricao   
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('grupo')
        return results
    },

    async findOneById(id) {
        const result = knex('grupo').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { nome, descricao } = data

            await knex('grupo')
                .where('id', id)
                .update({
                    nome: nome,
                    descricao: descricao
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('grupo').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}