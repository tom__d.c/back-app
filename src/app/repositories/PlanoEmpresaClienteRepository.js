const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { plano_empresa_id, cliente_id, status } = req.body

            await knex('plano_empresa_cliente').insert({
                plano_empresa_id,
                cliente_id,
                status
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('plano_empresa_cliente')
        return results
    },

    async findOneById(id) {
        const result = knex('plano_empresa_cliente').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { plano_empresa_id, cliente_id, status } = data

            await knex('plano_empresa_cliente')
                .where('id', id)
                .update({
                    plano_empresa_id: plano_empresa_id, 
                    cliente_id: cliente_id, 
                    status: status
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('plano_empresa_cliente').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}