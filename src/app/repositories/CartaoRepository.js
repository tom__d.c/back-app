const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { numero, cpf } = req.body

            await knex('cartao').insert({
                numero,
                cpf
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('cartao')
        return results
    },

    async findOneById(id) {
        const result = knex('cartao').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { numero, cpf } = data

            await knex('cartao')
                .where('id', id)
                .update({
                    numero: numero,
                    cpf: cpf
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('cartao').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}