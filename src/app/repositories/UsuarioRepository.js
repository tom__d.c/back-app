const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { nome, email, celular, senha, status, grupo_id } = req.body

            await knex('usuario').insert({
                nome,
                email,
                celular,
                senha,
                status,
                grupo_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('usuario')
        return results
    },

    async findOneById(id) {
        const result = knex('usuario').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { nome, email, celular, senha, status, grupo_id } = data

            await knex('usuario')
                .where('id', id)
                .update({
                    nome: nome,
                    email: email,
                    celular: celular,
                    senha: senha,
                    status: status,
                    grupo_id: grupo_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('usuario').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}