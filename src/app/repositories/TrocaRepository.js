const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { premiacao_id, cliente_id } = req.body

            await knex('troca').insert({
                premiacao_id, 
                cliente_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('troca')
        return results
    },

    async findOneById(id) {
        const result = knex('troca').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { premiacao_id, cliente_id } = data

            await knex('troca')
                .where('id', id)
                .update({
                    premiacao_id: premiacao_id,
                    cliente_id: cliente_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('troca').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}