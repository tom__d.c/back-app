const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { nome, descricao, valor, desconto, status, empresa_id } = req.body

            await knex('plano_empresa').insert({
                nome,
                descricao,
                valor,
                desconto,
                status,
                empresa_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('plano_empresa')
        return results
    },

    async findOneById(id) {
        const result = knex('plano_empresa').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { nome, descricao, valor, desconto, status, empresa_id } = data

            await knex('plano_empresa')
                .where('id', id)
                .update({
                    nome: nome,
                    descricao: descricao,
                    valor: valor,
                    desconto: desconto,
                    status: status,
                    empresa_id: empresa_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('plano_empresa').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}