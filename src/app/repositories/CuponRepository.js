const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { titulo, descricao, desconto, status, publico, empresa_id, usuario_id } = req.body

            await knex('cupon').insert({
                titulo, 
                descricao, 
                desconto, 
                status, 
                publico, 
                empresa_id, 
                usuario_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('cupon')
        return results
    },

    async findOneById(id) {
        const result = knex('cupon').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { titulo, descricao, desconto, status, publico, empresa_id, usuario_id } = data

            await knex('cupon')
                .where('id', id)
                .update({
                    titulo: titulo, 
                    descricao: descricao, 
                    desconto: desconto, 
                    status: status, 
                    publico: publico, 
                    empresa_id: empresa_id, 
                    usuario_id: usuario_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next){
        try {
            knex('cupon').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}