const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { acumulado, cliente_id } = req.body

            await knex('ponto').insert({
                acumulado,
                cliente_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('ponto')
        return results
    },

    async findOneById(id) {
        const result = knex('ponto').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { acumulado, cliente_id } = data

            await knex('ponto')
                .where('id', id)
                .update({
                    acumulado: acumulado,
                    cliente_id: cliente_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('ponto').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}