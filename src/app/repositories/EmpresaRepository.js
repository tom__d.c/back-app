const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { razao_social, nome_fantasia, cnpj, email, telefone1, telefone2, empresa, whatsapp,
                endereco_id, usuario_id, plano_sistema_id } = req.body

            await knex('empresa').insert({
                razao_social,
                nome_fantasia,
                cnpj,
                email,
                telefone1,
                telefone2,
                empresa,
                whatsapp,
                endereco_id,
                usuario_id,
                plano_sistema_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('empresa')
        return results
    },

    async findOneById(id) {
        const result = knex('empresa').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { id, razao_social, nome_fantasia, cnpj, email, telefone1, telefone2, empresa, whatsapp,
                endereco_id, usuario_id, plano_sistema_id } = data

            await knex('empresa')
                .where('id', id)
                .update({
                razao_social: razao_social,
                nome_fantasia: nome_fantasia,
                cnpj: cnpj,
                email: email,
                telefone1: telefone1,
                telefone2: telefone2,
                empresa: empresa,
                whatsapp: whatsapp,
                endereco_id: endereco_id,
                usuario_id: usuario_id,
                plano_sistema_id: plano_sistema_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next){
        try {
            knex('empresa').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}