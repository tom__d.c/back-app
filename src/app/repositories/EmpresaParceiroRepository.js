const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { empresa_id, parceiro_id } = req.body

            await knex('empresa_parceiro').insert({
                empresa_id, 
                parceiro_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('empresa_parceiro')
        return results
    },

    async findOneById(id) {
        const result = knex('empresa_parceiro').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { empresa_id, parceiro_id } = data

            await knex('empresa_parceiro')
                .where('id', id)
                .update({
                    empresa_id: empresa_id, 
                    parceiro_id: parceiro_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('empresa_parceiro').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}