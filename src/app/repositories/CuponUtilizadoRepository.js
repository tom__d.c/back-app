const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { cupon_id, cliente_id } = req.body

            await knex('cupon_utilizado').insert({
                cupon_id, 
                cliente_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('cupon_utilizado')
        return results
    },

    async findOneById(id) {
        const result = knex('cupon_utilizado').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { cupon_id, cliente_id } = data

            await knex('cupon_utilizado')
                .where('id', id)
                .update({
                    cupon_id: cupon_id,
                    cliente_id: cliente_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('cupon_utilizado').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}