const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { logradouro, numero, bairro, cep, cidade, estado } = req.body

            await knex('endereco').insert({
                logradouro,
                numero,
                bairro,
                cep,
                cidade,
                estado
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('endereco')
        return results
    },

    async findOneById(id) {
        const result = knex('endereco').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { logradouro, numero, bairro, cep, cidade, estado } = data

            await knex('endereco')
                .where('id', id)
                .update({
                    logradouro: logradouro,
                    numero: numero,
                    bairro: bairro,
                    cep: cep,
                    cidade: cidade,
                    estado: estado
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('endereco').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}