const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { empresa_id, cliente_id, valor, cupon_utilizado_id } = req.body

            await knex('compras').insert({
                empresa_id,
                cliente_id,
                valor,
                cupon_utilizado_id
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('compras')
        return results
    },

    async findOneById(id) {
        const result = knex('compras').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { empresa_id, cliente_id, valor, cupon_utilizado_id } = data

            await knex('compras')
                .where('id', id)
                .update({
                    empresa_id: empresa_id,
                    cliente_id: cliente_id,
                    valor: valor,
                    cupon_utilizado_id: cupon_utilizado_id
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('compras').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}