const knex = require('../database')

module.exports = {

    async create(req, res, next) {
        try {
            const { certificado_digital_banco_sicoob } = req.body

            await knex('configuracoes').insert({
                certificado_digital_banco_sicoob
            })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async findAll() {
        const results = knex.select().table('configuracoes')
        return results
    },

    async findOneById(id) {
        const result = knex('configuracoes').where('id', id)
        return result
    },

    async updateById(data, next) {
        try {
            const { certificado_digital_banco_sicoob } = data

            await knex('configuracoes')
                .where('id', id)
                .update({
                    certificado_digital_banco_sicoob: certificado_digital_banco_sicoob
                })

            return res.status(201).send()
        } catch (error) {
            next(error)
        }
    },

    async deleteById(id, next) {
        try {
            knex('configuracoes').where('id', id).del()
        } catch (error) {
            next(error)
        }
    }
}